#!/usr/bin/env bash

# Check URLs for aliveness

set -e

usage() {
  echo "Usage: $0 /path/to/file"
  exit 1
}

check() {
  while read -r url; do
    local result=$(curl -s -o /dev/null -L -w "%{http_code}" "$url")
    if [[ "$result" =~ [45].. ]]; then
      echo "$url is not alive"
      break
    fi
  done < "$filename"
}

if [[ $# -eq 1 ]]; then
  filename="$1"
else
  echo "Only one argument required!"
  usage
fi

# Is CURL available?
if ! $(type curl &>/dev/null); then
  echo -e "curl not found. Please install:\nhttps://curl.haxx.se/download.html"
  exit 1
fi

#Main
if [[ -f "$filename" && -r "$filename" ]]; then
  check filename
else
  usage
fi
