## URL checker
Проверка на "живость" URL'ов из списка
### Требования
Для работы скринта требуется установленная утилита [curl](https://curl.haxx.se/)
### Использование
```
$ bash checker.sh urls.txt
```
